var numArr = [];
document.getElementById('btnThemSo').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua');
    var txtNumber = document.getElementById('txt-number').value*1;
    numArr.push(txtNumber);
    ketQua.innerHTML = numArr;
    document.getElementById('txt-number').value = "";
});
//Tính tổng số dương
document.getElementById('btnTongSoDuong').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau1');
    var sum = 0;
    for(i = 0; i < numArr.length; i++){
        if(numArr[i] > 0){
            sum = sum + numArr[i];
        }
    }
    ketQua.innerHTML = sum;
})
//Đếm các số dương trong mảng
document.getElementById('btnDemSoDuong').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau2');
    var count = 0;
    for(i = 0; i < numArr.length; i++){
        if(numArr[i] > 0){
            count++
        }
    }
    ketQua.innerHTML = count;
})
//Tìm số nhỏ nhất trong mảng
document.getElementById('btnNhoNhat').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau3');
    var current = numArr[0];
    for(i = 0; i < numArr.length; i++){
        if(numArr[i] < current){
            current = numArr[i]
        }
    }
    ketQua.innerHTML = current;
})

//Tìm số dương nhỏ nhất trong mảng
document.getElementById('btnDuongNhoNhat').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau4');
    var arrDuong = [];
    var numberMin = -1;
    for(i = 0; i < numArr.length; i++){
        if(numArr[i]>0){
            arrDuong.push(numArr[i]);
        }
    }
    // if(arrDuong.indexOf() == -1){
    //     ketQua.innerHTML = "Không có phần tử dương nào trong máng";
    // }else{
        for( i = 0 ; i < arrDuong.length; i++){
            if((numberMin == -1 || numberMin > numArr[i]) && numArr[i] >0){
                numberMin = arrDuong[i];
            }
        }
        ketQua.innerHTML = numberMin;
    // }
})
//Tìm số chẵn cuối cùng
document.getElementById('btnChanCuoiCung').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau5');
    var numberChan = 0;
    
    for(i = 0 ; i < numArr.length ; i++){
        if(numArr[i] % 2 == 0 ){
            numberChan = numArr[i];
        }
    }
    ketQua.innerHTML = numberChan;
})
// Đổi chỗ 2 phần tử (Vị trí phần tử bắt đầu từ 0)
document.getElementById('btnDoiCho').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau6');
    var number1 = document.getElementById('phan-tu1').value*1;
    var number2 = document.getElementById('phan-tu2').value*1;
    var n = 0;
    
        n = numArr[number1];
        numArr[number1] = numArr[number2];
        numArr[number2] = n;

    ketQua.innerHTML = numArr;
    
});
//tăng dần
document.getElementById('btnTangDan').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau7');
    ketQua.innerHTML = numArr.sort();
});
//Tìm số nguyên tố đầu tiên
function soNguyenTo(n){
    var flag = true;
    if (n < 2) {
        flag = false;
        return flag;
    }
    for (var i = 2; i < n; i++) {
        if (n % i == 0) {
            flag = false;
            break;
        }
    }
    return flag;
}
document.getElementById('btnNguyenToDauTien').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau8');
    var soNguyenToDauTien = 0;
    for(i = 0 ; i < numArr.length ; i++){
        if(soNguyenTo(numArr[i])==true){
            soNguyenToDauTien = numArr[i];
            break;
        }else{
            soNguyenToDauTien = -1;
        }
    }
    ketQua.innerHTML = `${soNguyenToDauTien}`
});

//Đếm số nguyên
document.getElementById('btnDemSoNguyen').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau9');
    var count = 0;
    for(var i = 0 ; i < numArr.length ; i++){
        if(Number.isInteger(numArr[i])){
            count++;
        }
    }
    ketQua.innerHTML = `${count}`
  

});

//So sánh số dương và âm
document.getElementById('btnSoSanh').addEventListener('click',function(){
    var ketQua = document.getElementById('ket-qua-cau10');
    var soAm = 0;
    var soDuong = 0;

    for(i = 0; i < numArr.length ; i++){
        if(numArr[i] > 0){
            soDuong++;
        }
        if(numArr[i] < 0){
            soAm++;
        }
    }
    console.log(soAm,soDuong)
    if(soDuong > soAm){
        ketQua.innerHTML = "Số Dương > Số Âm";
    }else if(soDuong < soAm){
        ketQua.innerHTML = "Số Dương < Số Âm";
    }else{
        ketQua.innerHTML = "Số Dương = Số Âm";
    }
});